#!/bin/bash

# LITTLE SCRIPT TO SET A TIMER AND STRETCH YOUR LEGS EVERY ONCE IN A WHILE

# VARIABLES
SCRIPT_PATH="`dirname \"$0\"`"
source $SCRIPT_PATH/configPomodoro.conf

red='\E[31;31m'
NC='\E[0m'
numberRep='1'
offset='0'


# MAIN FUNCTION

function commands {
    clear
    echo -e $red    "                                                   /SS                              ";
    echo            "                                                  | SS                              ";
    echo            "  /SSSSSS   /SSSSSS  /SSSSSS/SSSS   /SSSSSS   /SSSSSSS  /SSSSSS   /SSSSSS   /SSSSSS ";
    echo            " /SS__  SS /SS__  SS| SS_  SS_  SS /SS__  SS /SS__  SS /SS__  SS /SS__  SS /SS__  SS";
    echo            "| SS  \ SS| SS  \ SS| SS \ SS \ SS| SS  \ SS| SS  | SS| SS  \ SS| SS  \__/| SS  \ SS";
    echo            "| SS  | SS| SS  | SS| SS | SS | SS| SS  | SS| SS  | SS| SS  | SS| SS      | SS  | SS";
    echo            "| SSSSSSS/|  SSSSSS/| SS | SS | SS|  SSSSSS/|  SSSSSSS|  SSSSSS/| SS      |  SSSSSS/";
    echo            "| SS____/  \______/ |__/ |__/ |__/ \______/  \_______/ \______/ |__/       \______/ ";
    echo            "| SS                                                                                ";
    echo            "| SS                                                                                ";
    echo            "|__/                                                                                ";
    echo -e $NC     "   /SS     /SS                                                                      ";
    echo            "  | SS    |__/                                                                      ";
    echo            " /SSSSSS   /SS /SSSSSS/SSSS   /SSSSSS   /SSSSSS                                     ";
    echo            "|_  SS_/  | SS| SS_  SS_  SS /SS__  SS /SS__  SS                                    ";
    echo            "  | SS    | SS| SS \ SS \ SS| SSSSSSSS| SS  \__/                                    ";
    echo            "  | SS /SS| SS| SS | SS | SS| SS_____/| SS                                          ";
    echo            "  |  SSSS/| SS| SS | SS | SS|  SSSSSSS| SS                                          ";
    echo            "   \___/  |__/|__/ |__/ |__/ \_______/|__/                                          ";
    echo            "                                                                                    ";
    echo            "                                                                                    ";
    echo
    
    echo -e $red "What would you like to do?" $NC

    select option in "Setup" "Start_Day" "Start_Once" "Stop" "Exit"; do 
	case $option in
	    Setup ) clear; setup; break;;
	    Start_Day ) start; break;;
	    Start_Once ) startOnce; break;;
	    Stop ) stop; break;;
            Exit ) quit; exit; break;;
	esac
    done
}

function setup {
    select option in "Set_Work_Time" "Set_Work_Session_Time" "Set_Pause_Time" "Set_Default_Sound" "Back"; do 
	case $option in	
            Set_Work_Time ) read -p "Enter your total work time: " totalWorkTime;			
			    sed -i -r "s/^(TOTAL_WORK_TIME=).*/\1'$totalWorkTime'/" config.conf; break;;
	    Set_Work_Session_Time )  read -p "Enter your work time session: " workSession;				 
				     sed -i -r "s/^(WORK_SESSION_TIME=).*/\1'$workSession'/" config.conf; break;;
	    Set_Pause_Time ) read -p "Enter your pause session time: " pauseSession;
			     sed -i -r "s/^(PAUSE_SESSION_TIME=).*/\1'$pauseSession'/" config.conf; break;; 
	    Set_Default_Sound ) read -p "Enter your favorite sound (Absolute Path): " favoriteSound;			    
				sed -i -r "s#^(DEFAULT_SOUND=).*#\1'$favoriteSound'#" config.conf; break;;          
            Back ) clear; commands; break;;
	esac
    done
}

function start {

    numberRep=$(($TOTAL_WORK_TIME*60/($WORK_SESSION_TIME+$PAUSE_SESSION_TIME)));
    globalPopupTime=$(($WORK_SESSION_TIME+$PAUSE_SESSION_TIME));
    
    echo $numberRep;

    for (( i=1 ; i<=$numberRep ; i++ )); 
    do
	
	echo "notify-send -w -u critical PomodoroTimer BREAK & mpv $DEFAULT_SOUND" | at now + $(($offset+$WORK_SESSION_TIME)) minutes;
	echo "notify-send -w -u critical PomodoroTimer WORK! & mpv $DEFAULT_SOUND" | at now + $(($offset+$globalPopupTime)) minutes;
	offset=$(($offset+$globalPopupTime));   

    done
    
}

function startOnce {

    globalPopupTime=$(($WORK_SESSION_TIME+$PAUSE_SESSION_TIME));
    
    echo "notify-send -w -u critical PomodoroTimer BREAK & mpv $DEFAULT_SOUND" | at now + $WORK_SESSION_TIME minutes;
    echo "notify-send -w -u critical PomodoroTimer WORK! & mpv $DEFAULT_SOUND" | at now + $globalPopupTime minutes;
    offset=$(($offset+$globalPopupTime));   
    
}

function stop {


    echo "WARNING THIS WILL REMOVE ALL PENDING NOTIFICATIONS";
    echo "are you sure to continue?";
    read -p "Enter Y or N: " answer;
    
    if [[ $answer == 'Y' ]]
    then
	
	for i in `atq | awk '{print $1}'`;do atrm $i;done
	echo "All notifications removed correctly!";
	sleep 1;
	
    elif [[ $answer == 'N' ]]
    then
	echo "Aborting operation...";
	sleep 1;

    fi
}


function quit {

 	
	for i in `atq | awk '{print $1}'`;do atrm $i;done
	echo "All notifications removed correctly!";
	sleep 1;
	kill $$;
}

commands
while commands
do    
    commands
done

