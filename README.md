# pomodoroTimer

Simple pomodoro timer technique to organize your work day. Written in bash.

![Screenshot](screenshot.jpeg)

# Requirements

at and mpv and notify-osd/notify-send are required to run this script

# Config

Before starting the program check how the config file works. You'll find 4 variables.
The default sound variable must be modified to have everything working properly
